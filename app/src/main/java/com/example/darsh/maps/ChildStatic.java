package com.example.darsh.maps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 * Created by Darsh on 10/09/2016.
 */
public class ChildStatic extends AppCompatActivity{


    public static String User_Name;
    public static String User_Phone;
    public static String User_Local_Phone;
    public static String User_Place;
    public static String User_Email;
    public static String User_Nationality;
    public static String User_Place2;
    public static String Health_Illness1;
    public static String Health_Drugs1;
    public static String Health_the_Number_of_Doses1;
    public static String Health_Doses1;
    public static String Health_Illness2;
    public static String Health_Drugs2;
    public static String Health_the_Number_of_Doses2;
    public static String Health_Doses2;
    public static String Health_Illness3;
    public static String Health_Drugs3;
    public static String Health_the_Number_of_Doses3;
    public static String Health_Doses3;
    public static String Health_Illness4;
    public static String Health_Drugs4;
    public static String Health_the_Number_of_Doses4;
    public static String Health_Doses4;
    public static String Health_Illness5;
    public static String Health_Drugs5;
    public static String Health_the_Number_of_Doses5;
    public static String Health_Doses5;
    public static String Health_Illness6;
    public static String Health_Drugs6;
    public static String Health_the_Number_of_Doses6;
    public static String Health_Doses6;
    public static String Health_Illness7;
    public static String Health_Drugs7;
    public static String Health_the_Number_of_Doses7;
    public static String Health_Doses7;
    public static String Health_Illness8;
    public static String Health_Drugs8;
    public static String Health_the_Number_of_Doses8;
    public static String Health_Doses8;
    public static String Health_Illness9;
    public static String Health_Drugs9;
    public static String Health_the_Number_of_Doses9;
    public static String Health_Doses9;
    public static String Health_Illness10;
    public static String Health_Drugs10;
    public static String Health_the_Number_of_Doses10;
    public static String Health_Doses10;
    public static String Rafiq_Name1;
    public static String Rafiq_Phone1;
    public static String Rafiq_Relation1;
    public static String Rafiq_Name2;
    public static String Rafiq_Phone2;
    public static String Rafiq_Relation2;
    public static String Rafiq_Name3;
    public static String Rafiq_Phone3;
    public static String Rafiq_Relation3;
    public static String Rafiq_Name4;
    public static String Rafiq_Phone4;
    public static String Rafiq_Relation4;
    public static String Rafiq_Name5;
    public static String Rafiq_Phone5;
    public static String Rafiq_Relation5;
    public static String Wsya_Where_Buried;
    public static String Wsya_who_assumes_that;
    public static String Wsya_Phone_Number;
    public static String Wsya_Recommendation;
    public static String Food_Food1;
    public static String Food_Food2;
    public static String Food_Food3;
    public static String Food_Food4;
    public static String Food_Food5;

//    save data SharedPreferences

    public static final String FILE_PREF_NAME = "hoop";
    public static final String FILE_PREF_NAME2 = "hoop2";

    // api_Token

    public static String API_TOKEN = "api.txt";

    //    save data from this files

    public static String USER_NAME = "username.txt";
    public static String USER_PHONE = "userphone.txt";
    public static String USER_LOCAL_PHONE = "userlocalphone.txt";
    public static String USER_PLACE = "userplace.txt";
    public static String USER_EMAIL = "useremail.txt";
    public static String USER_NATIONALITY = "usernationality.txt";
    public static String USER_PLACE2 = "userplace2.txt";
    public static String HEALTH_ILLNESS1 = "healthillness1.txt";
    public static String HEALTH_DRUGS1 = "healthdrugs1.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES1 = "healththenuberofdoses1.txt";
    public static String HEALTH_DOSES1 = "healthdoses1.txt";
    public static String HEALTH_ILLNESS2 = "healthillness2.txt";
    public static String HEALTH_DRUGS2 = "healthdrugs2.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES2 = "healththenuberofdoses2.txt";
    public static String HEALTH_DOSES2 = "healthdoses2.txt";
    public static String HEALTH_ILLNESS3 = "healthillness3.txt";
    public static String HEALTH_DRUGS3 = "healthdrugs3.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES3 = "healththenuberofdoses3.txt";
    public static String HEALTH_DOSES3 = "healthdoses3.txt";
    public static String HEALTH_ILLNESS4 = "healthillness4.txt";
    public static String HEALTH_DRUGS4 = "healthdrugs4.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES4 = "healththenuberofdoses4.txt";
    public static String HEALTH_DOSES4 = "healthdoses4.txt";
    public static String HEALTH_ILLNESS5 = "healthillness5.txt";
    public static String HEALTH_DRUGS5 = "healthdrugs5.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES5 = "healththenuberofdoses5.txt";
    public static String HEALTH_DOSES5 = "healthdoses5.txt";
    public static String HEALTH_ILLNESS6 = "healthillness6.txt";
    public static String HEALTH_DRUGS6 = "healthdrugs6.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES6 = "healththenuberofdoses6.txt";
    public static String HEALTH_DOSES6 = "healthdoses6.txt";
    public static String HEALTH_ILLNESS7 = "healthillness7.txt";
    public static String HEALTH_DRUGS7 = "healthdrugs7.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES7 = "healththenuberofdoses7.txt";
    public static String HEALTH_DOSES7 = "healthdoses7.txt";
    public static String HEALTH_ILLNESS8 = "healthillness8.txt";
    public static String HEALTH_DRUGS8 = "healthdrugs8.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES8 = "healththenuberofdoses8.txt";
    public static String HEALTH_DOSES8 = "healthdoses8.txt";
    public static String HEALTH_ILLNESS9 = "healthillness9.txt";
    public static String HEALTH_DRUGS9 = "healthdrugs9.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES9 = "healththenuberofdoses9.txt";
    public static String HEALTH_DOSES9 = "healthdoses9.txt";
    public static String HEALTH_ILLNESS10 = "healthillness10.txt";
    public static String HEALTH_DRUGS10 = "healthdrugs10.txt";
    public static String HEALTH_THE_NUMBER_OF_DOSES10 = "healththenuberofdoses10.txt";
    public static String HEALTH_DOSES10 = "healthdoses10.txt";
    public static String RAFIQ_NAME1 = "rafiqname1.txt";
    public static String RAFIQ_PHONE1 = "rafiqphone1.txt";
    public static String RAFIQ_RELATION1 = "rafiqrelation1.txt";
    public static String RAFIQ_NAME2 = "rafiqname2.txt";
    public static String RAFIQ_PHONE2 = "rafiqphone2.txt";
    public static String RAFIQ_RELATION2 = "rafiqrelation2.txt";
    public static String RAFIQ_NAME3 = "rafiqname3.txt";
    public static String RAFIQ_PHONE3 = "rafiqphone3.txt";
    public static String RAFIQ_RELATION3 = "rafiqrelation3.txt";
    public static String RAFIQ_NAME4 = "rafiqname4.txt";
    public static String RAFIQ_PHONE4 = "rafiqphone4.txt";
    public static String RAFIQ_RELATION4 = "rafiqrelation4.txt";
    public static String RAFIQ_NAME5 = "rafiqname5.txt";
    public static String RAFIQ_PHONE5 = "rafiqphone5.txt";
    public static String RAFIQ_RELATION5 = "rafiqrelation5.txt";
    public static String WSYA_WHERE_BURIED = "wsyawhereburied.txt";
    public static String WSYA_WHO_ASSUMES_THAT = "wsyawhoassumesthat.txt";
    public static String WSYA_PHONE_NUMBER = "wsyaphonenumber.txt";
    public static String WSYA_RECOMMENDATION = "wsyarecommendation.txt";
    public static String FOOD_FOOD1 = "food1.txt";
    public static String FOOD_FOOD2 = "food2.txt";
    public static String FOOD_FOOD3 = "food3.txt";
    public static String FOOD_FOOD4 = "food4.txt";
    public static String FOOD_FOOD5 = "food5.txt";

    // add language set and get
    public static String LANG_FILE = "langfile.txt";
    public static String LANG = LANG_FILE.toString();

    public static String getLANG() {
        return LANG;
    }

    public static String setLANG(String LANG) {
        ChildStatic.LANG = LANG;
        return LANG;
    }





    public static String read(Activity activity, String file) {
        String read = "";
        try {
            FileInputStream stream = activity.openFileInput(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer b = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                b.append(line);
            }
            read = b.toString();
            stream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return read;
    }

    public static void write(Context context, String file, String str, EditText et) {
        str = et.getText().toString();

        try {
            FileOutputStream fos = context.openFileOutput(file, 0);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeLANG(Context context, String file, String str) {
        try {
            FileOutputStream fos = context.openFileOutput(file, 0);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeSP(Context context, String file, String str, Spinner sp) {
        str = sp.getSelectedItem().toString();

        try {
            FileOutputStream fos = context.openFileOutput(file, 0);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeAPI(Context context, String file, String str) {
        try {
            FileOutputStream fos = context.openFileOutput(file, 0);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void setLocal(Context context , String language){
        Locale myLocal = new Locale(language);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocal;
        res.updateConfiguration(conf, dm);

    }


}

