/*
package com.example.darsh.maps;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;


*/
/**
 * Created by Darsh on 27/09/2016.
 *//*

public class LocationService extends Service {

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        final LocationManager mlocmag = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final LocationListener mlocList = new MyLocationList();
        final Location loc = mlocmag.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        UpdateWithNewLocation(loc); // This method is used to get updated location.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mlocmag.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocList);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
    }


    private void UpdateWithNewLocation(final Location loc) {
        // TODO Auto-generated method stub

        if(loc!= null)
        {
            final double lat =loc.getLatitude(); // Updated lat
            final double Long = loc.getLongitude(); // Updated long


            ConnectMySQL obj = new ConnectMySQL();
            obj.call(lat,Long); // Call this method when location is updated and save the data.

        }

        else
        {
            String latLongStr = "No lat and longitude found";
            Toast.makeText(this, "Your location is "+latLongStr ,Toast.LENGTH_LONG).show();
        }


    }



    public class MyLocationList implements LocationListener
    {

        public void onLocationChanged(Location arg0) {
            // TODO Auto-generated method stub
            UpdateWithNewLocation(arg0);
        }

        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
            Toast.makeText(getApplicationContext(),"GPS Disable ", Toast.LENGTH_LONG).show();
        }

        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
            Toast.makeText(getApplicationContext(),"GPS enabled", Toast.LENGTH_LONG).show();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }

    }*/
