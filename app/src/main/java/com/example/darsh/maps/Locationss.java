package com.example.darsh.maps;


import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.List;


/**
 * Created by Darsh on 23/10/2016.
 */
public class Locationss extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {


    EditText et;
    Context ctx;
    String Location, helwan1;
    private static final int GPS_ERRORDIALOG_REQUEST = 9001;
    private static final float DEFAULTZOOM = 15;
    static GoogleMap mMap;
    Marker marker1;
    Marker marker2;
    ImageView mark;

    private GoogleApiClient mGoogleApiClient;
    Location currentLocation;

    LocationRequest mLocationRequest;
    Polyline line;


    LatLng helwan = new LatLng(29.848571704678655, 31.333872489631172);
    LatLng mansour = new LatLng(29.847467532177394, 31.333975084126);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.your_location);

        if (ServicesOk()) {
            setContentView(R.layout.your_location);
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            checkLocationPermission();
            if (initMap()) {
                Toast.makeText(this, "Ready to Map", Toast.LENGTH_SHORT).show();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkLocationPermission();
                }
            } else {
                Toast.makeText(this, "Map not available ", Toast.LENGTH_SHORT).show();
            }

            buildGoogleApiClient();




        } else {
            setContentView(R.layout.activity_maps);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }

   /* public boolean Server(){
        setContentView(R.layout.activity_maps);
        return true;
    }*/




    public boolean ServicesOk() {
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable, this, GPS_ERRORDIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "Can't Connect to Google Play Services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void getMaps(){
        if (mMap != null) {
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.info_window, null);
                    TextView tvLocality = (TextView) v.findViewById(R.id.tv_locality);
                    TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);
                    TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);
                    TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);

                    LatLng ll = marker.getPosition();
                    tvLocality.setText(marker.getTitle());
                    tvLat.setText("Lat : " + ll.latitude);
                    tvLng.setText("Lng : " + ll.longitude);
                    tvSnippet.setText(marker.getSnippet());
                    return v;
                }
            });

            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng ll) {

                    Geocoder gc = new Geocoder(Locationss.this);
                    List<Address> list = null;
                    try {
                        list = gc.getFromLocation(ll.latitude, ll.longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                    Address add = list.get(0);
                    Locationss.this.setMarker(add.getLocality(), add.getCountryName(), ll.latitude, ll.longitude);

                }
            });
            /* // this toast to show you Details for location when you touch screen
               mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        String msg = marker.getTitle() + " (" +marker.getPosition().latitude +
                                "," +marker.getPosition().longitude + ")";
                        Toast.makeText(MapsActivity.this, msg, Toast.LENGTH_LONG).show();
                        return false;
                    }
                });*/

            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    Geocoder gc = new Geocoder(Locationss.this);
                    List<Address> list = null;
                    LatLng ll = marker.getPosition();
                    try {
                        list = gc.getFromLocation(ll.latitude, ll.longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                    Address add = list.get(0);
                    marker.setTitle(add.getLocality());
                    marker.setSnippet(add.getCountryName());
                    marker.showInfoWindow();
                }
            });
        }
    }

    private boolean initMap() {
        if (mMap == null) {
            final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
           mapFragment.getMapAsync(this);
//            mMap = mapFragment.getMap();
            if (mMap != null) {
                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        View v = getLayoutInflater().inflate(R.layout.info_window, null);
                        TextView tvLocality = (TextView) v.findViewById(R.id.tv_locality);
                        TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);
                        TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);
                        TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);

                        LatLng ll = marker.getPosition();
                        tvLocality.setText(marker.getTitle());
                        tvLat.setText("Lat : " + ll.latitude);
                        tvLng.setText("Lng : " + ll.longitude);
                        tvSnippet.setText(marker.getSnippet());
                        return v;
                    }
                });

                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng ll) {

                        Geocoder gc = new Geocoder(Locationss.this);
                        List<Address> list = null;
                        try {
                            list = gc.getFromLocation(ll.latitude, ll.longitude, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return;
                        }
                        Address add = list.get(0);
                        Locationss.this.setMarker(add.getLocality(), add.getCountryName(), ll.latitude, ll.longitude);

                    }
                });
            /* // this toast to show you Details for location when you touch screen
               mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        String msg = marker.getTitle() + " (" +marker.getPosition().latitude +
                                "," +marker.getPosition().longitude + ")";
                        Toast.makeText(MapsActivity.this, msg, Toast.LENGTH_LONG).show();
                        return false;
                    }
                });*/

                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        Geocoder gc = new Geocoder(Locationss.this);
                        List<Address> list = null;
                        LatLng ll = marker.getPosition();
                        try {
                            list = gc.getFromLocation(ll.latitude, ll.longitude, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return;
                        }
                        Address add = list.get(0);
                        marker.setTitle(add.getLocality());
                        marker.setSnippet(add.getCountryName());
                        marker.showInfoWindow();
                    }
                });
            }
        }
        return (mMap != null);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override

    public void onMapReady(GoogleMap googleMap) { //30.0444° N, 31.2357° E
        mMap = googleMap;

        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                Double lat = cameraPosition.target.latitude;
                Double lng = cameraPosition.target.latitude;

                if (marker1 != null) {
                    marker1.remove();
                }

                LatLng ddd = new LatLng(lat , lng);

                if (marker1 == null) {
                    marker1 = mMap.addMarker(new MarkerOptions().position(ddd));
                    InfoAdapter();
                } /*else {
                    removeEverything2();
                    marker1 = mMap.addMarker(new MarkerOptions().position(ddd));
                }*/



                Log.i("centerLat", Double.toString(lat));

                Log.i("centerLong", Double.toString(lng));
            }
        });



       goToLocation(29.848996, 31.334211, DEFAULTZOOM);
//        LatLng ddd = mMap.getProjection().getVisibleRegion().latLngBounds.getCenter();
//         Add a marker in Sydney and move the camera
//        mMap.addMarker(new MarkerOptions().position(helwan).title("Helwan"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(helwan, 18));
//        mMap.setMyLocationEnabled(true);

        ;


    }

    public void InfoAdapter() {
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.info_window, null);
                TextView tvLocality = (TextView) v.findViewById(R.id.tv_locality);
                TextView tvLat = (TextView) v.findViewById(R.id.tv_lat);
                TextView tvLng = (TextView) v.findViewById(R.id.tv_lng);
                TextView tvSnippet = (TextView) v.findViewById(R.id.tv_snippet);

                LatLng ll = marker.getPosition();
                tvLocality.setText(marker.getTitle());
                tvLat.setText("Lat : " + ll.latitude);
                tvLng.setText("Lng : " + ll.longitude);
                tvSnippet.setText(marker.getSnippet());
                return v;
            }
        });
    }

    public void geoLocate(View v) throws IOException {

        hideSoftKeyboard(v);


        et = (EditText) findViewById(R.id.et_search);
        Location = et.getText().toString();

        Geocoder ge = new Geocoder(this);
        List<Address> list = ge.getFromLocationName(Location, 1);
        Address add = list.get(0);
        String locality = add.getLocality();
        String country = add.getCountryName();

        Toast.makeText(this, locality, Toast.LENGTH_LONG).show();
        double lat = add.getLatitude();
        double lng = add.getLongitude();

        goToLocation(lat, lng, DEFAULTZOOM);
        setMarker(locality, country, lat, lng);
//        setMarker2(locality, country, lat, lng);


    }

    private void hideSoftKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void goToLocation(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mMap.moveCamera(update);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mapNone:
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                break;
            case R.id.mapHybrid:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.mapSatellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTerrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.mapNormal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    // to get your Location you need handle two method  buildGoogleApiClient() and goToCurrentLocation()

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    protected void goToCurrentLocation() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        GPS_ERRORDIALOG_REQUEST);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        GPS_ERRORDIALOG_REQUEST);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this, "Connected to location Service ", Toast.LENGTH_LONG).show();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    /*@Override
    protected void onStop() {
        super.onStop();
        MapStateManager mgr = new MapStateManager(this);
        mgr.saveMapState(mMap);
    }*/

    /*@Override
    protected void onResume() {
        super.onResume();
        MapStateManager mgr = new MapStateManager(this);
        CameraPosition position = mgr.getSavedCameraPosition();
        if (position != null) {
            CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
            mMap.moveCamera(update);
        }

    }*/

    private void setMarker(String locality, String country, double lat, double lng) {

        BitmapDescriptor go = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
//        BitmapDescriptor no = BitmapDescriptorFactory.fromResource(R.drawable.ic_cast_dark);

        if (marker1 != null) {
            marker1.remove();
        }


        MarkerOptions options = new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title(locality)
                .icon(go)
                .draggable(true);
        if (country.length() > 0) {
            options.snippet(country);
        }

        if (marker1 == null) {
            marker1 = mMap.addMarker(options);

        } else if (marker2 == null) {
            marker2 = mMap.addMarker(options);
            drawLine();
        } else {
            removeEverything();
            marker1 = mMap.addMarker(options);
        }
    }

    /*  private void setMarker2(String locality , String country ,  double lat , double lng ){

          BitmapDescriptor go = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
          BitmapDescriptor no = BitmapDescriptorFactory.fromResource(R.drawable.ic_cast_dark);


          MarkerOptions options = new MarkerOptions()
                  .position(helwan)
                  .title(locality)
                  .icon(go)
                  .draggable(true);
          MarkerOptions options1 = new MarkerOptions()
                  .position(mansour)
                  .title(locality)
                  .icon(go)
                  .draggable(true);
          if(country.length() > 0){
              options.snippet(country);
          }

          if(marker1 == null){
              marker1 = mMap.addMarker(options);

          }
          else if (marker2 == null){
              marker2 = mMap.addMarker(options1);
              helwanLocation();
          }
          else {
              removeEverything();
              marker1 = mMap.addMarker(options);
          }
      }
  */
    private void drawLine() {

        PolylineOptions options = new PolylineOptions();

        options.add(marker1.getPosition())
                .add(marker2.getPosition())
                .color(Color.BLUE)
                .width(5);


        line = mMap.addPolyline(options);
    }

    private void removeEverything() {
        marker1.remove();
        marker1 = null;
        marker2.remove();
        marker2 = null;
        line.remove();

    }

    private void removeEverything2() {
        marker1.remove();
        marker1 = null;
    }

    private void helwanLocation() {

        PolylineOptions options = new PolylineOptions();
        options.add(
                helwan,
                new LatLng(29.848722338800787, 31.336825601756573),
                new LatLng(29.848718849209774, 31.33722022175789),
                new LatLng(29.847639396542604, 31.33722558617592),
                mansour
        )
                .width(5)
                .color(Color.GREEN);
        line = mMap.addPolyline(options);
    }


    @Override
    public void onLocationChanged(Location location) {

    }
}