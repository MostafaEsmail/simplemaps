package com.example.darsh.maps;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.JsonReader;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Darsh on 04/11/2016.
 */
public class LocationSnapper {
    public final List<LatLng> org = new ArrayList<>();
    public final List<LatLng> results = new ArrayList<>();

    public Location lastLocation;

    public final Context context;

    public interface Callback {
        public void changed();
    }

    public LocationSnapper(Context context) {
        this.context = context;
    }

    public Callback callback;

    public void addLocation(Location l) {
        org.add(new LatLng(l.getLatitude(), l.getLongitude()));
        new APICaller().execute();
    }

    class APICaller extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (org.size() == 0) return null;
            try {
                StringBuilder builder = new StringBuilder();
                builder.append("https://roads.googleapis.com/v1/snapToRoads?path=");
                for (LatLng l : org) {
                    builder.append(l.latitude)
                            .append(',')
                            .append(l.longitude)
                            .append('|');
                }
                builder.deleteCharAt(builder.length() - 1);
                builder.append("&interpolate=true&key=" + context.getString(R.string.APIKEYROADS));
                HttpURLConnection conn = (HttpURLConnection) new URL(builder.toString()).openConnection();
                conn.setRequestMethod("GET");
                conn.setUseCaches(false);
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(20000);
                conn.connect();

                JsonReader reader = new JsonReader(new InputStreamReader(conn.getInputStream()));
                reader.beginObject();
                while(reader.hasNext()) {
                    if ("snappedPoints".equals(reader.nextName())) {
                        reader.beginArray();
                        while(reader.hasNext()) {
                            reader.beginObject();
                            while(reader.hasNext()) {
                                if ("location".equals(reader.nextName())) {
                                    reader.beginObject();
                                    double lat = 0, lng = 0;
                                    while(reader.hasNext()) {
                                        String name = reader.nextName();
                                        if ("latitude".equals(name)) {
                                            lat = reader.nextDouble();
                                        } else if ("longitude".equals(name)) {
                                            lng = reader.nextDouble();
                                        } else {
                                            reader.skipValue();
                                        }
                                    }
                                    synchronized (results) {
                                        results.add(new LatLng(lat, lng));
                                    }
                                    reader.endObject();
                                } else {
                                    reader.skipValue();
                                }
                            }
                            reader.endObject();
                        }
                        reader.endArray();
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (callback != null) { callback.changed(); }
        }
    }
}
